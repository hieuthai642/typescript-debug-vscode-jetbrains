# Typescript Debug Template

- Example config for debugging Typescript using VSCode / JetBrains

- To watch the change

```bash
npm run watch:build
```

- If we run the above command, no need to config rebuild in VSCode / JetBrains. Otherwise

  - VScode: enable `preLaunchTask` in `.vscode/launch.json`
  - JetBrains: `Settings/Preferences` (Ctrl+Alt+S), go to `Languages and Frameworks | TypeScript`, and select the `Recompile on changes` checkbox.

- To make `nodemon` watch for file changes, you can make it watch the build directory.
